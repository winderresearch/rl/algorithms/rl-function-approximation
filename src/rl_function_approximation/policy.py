"""Policy related functions."""
from typing import Callable

import gym  # type: ignore
import numpy as np  # type: ignore


def argmax_ϵ_greedy(π: np.ndarray, ϵ: float, env: gym.Env) -> np.int64:
    """Returns the argmax of action probabilities, breaking ties randomly."""
    A = np.random.choice(np.flatnonzero(π == π.max()))
    if np.random.uniform() < ϵ:
        A = env.action_space.sample()
    return A


def π(S: np.ndarray, θ: np.ndarray, ϕ: Callable) -> np.ndarray:
    """Returns action-values for a parameterised linear approximation."""
    return np.array(np.dot(θ.T, ϕ(S)))
