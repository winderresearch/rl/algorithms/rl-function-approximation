"""Functions to convert state to features."""
from typing import Callable

import numpy as np  # type: ignore


def linear_ϕ(S: np.ndarray) -> np.ndarray:
    """Linear encoder for all actions."""
    return np.append(S, 1)  # Append bias term


def ϕ_drop(indices_to_keep: np.ndarray, ϕ: Callable) -> Callable:
    """Drop states from observation."""

    def drop(S: np.ndarray) -> np.ndarray:
        S = S[indices_to_keep]
        return ϕ(S)

    return drop


def one_hot_ϕ(n_obs: int) -> Callable:
    """Create one-hot encoder for the number of observations."""

    def ϕ(S: int) -> np.ndarray:
        res = np.zeros((n_obs,))
        res[S] = 1
        return np.append(res, 1)  # Append bias term

    return ϕ
