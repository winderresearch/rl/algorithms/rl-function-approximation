"""Implements the GTD reinforcement learning algorithm."""
from __future__ import annotations

import tempfile

import gym  # type: ignore
from gym import logger  # type: ignore
import gym_simple_cliffworld  # type: ignore # noqa: F401
import numpy as np  # type: ignore

from rl_function_approximation import features
from rl_function_approximation.policy import argmax_ϵ_greedy, π


class GTD(object):
    """An implementation of GTD(0).

    From: Sutton, Richard S, Hamid R. Maei, and Csaba Szepesvári. 2009.
    'A Convergent O(n) Temporal-Difference Algorithm for Off-Policy Learning
    with Linear Function Approximation'.
    In Advances in Neural Information Processing Systems 21,
    edited by D. Koller, D. Schuurmans, Y. Bengio, and L. Bottou, 1609–1616.
    Curran Associates, Inc.
    """

    def __init__(self: GTD, env: gym.Env, ϵ: float) -> None:
        """Initialisation function.

        Args: # noqa: DAR101
            env: An openai-gym environment.
            ϵ: The exploration factor. # noqa: DAR102
        """
        if isinstance(env.observation_space, gym.spaces.Discrete):
            n_obs = env.observation_space.n
            n_actions = env.action_space.n
            self.ϕ = features.one_hot_ϕ(n_obs)
            n_feats = len(self.ϕ(0))
        else:
            n_obs = env.observation_space.shape[0]
            n_actions = env.action_space.n
            self.ϕ = features.ϕ_drop(np.array([2, 3]), features.linear_ϕ)
            n_feats = len(self.ϕ(np.zeros(n_obs)))

        self.env = env
        self.θ = np.zeros((n_feats, n_actions))
        self.u = np.zeros((n_feats, n_actions))
        self.γ = 0.95
        self.α = 0.1
        self.β = 0.1
        self.ϵ = ϵ

    def episode(self: GTD) -> None:
        """Run an episode of the environment."""
        total_reward = 0
        S = env.reset()
        done = False
        while not done:
            # Choose Action
            A = argmax_ϵ_greedy(π(S, self.θ, self.ϕ), self.ϵ, self.env)

            # Take action and observe next state
            S_1, R, done, _ = self.env.step(A)
            total_reward += R

            # Update TD Error
            A_1 = argmax_ϵ_greedy(π(S_1, self.θ, self.ϕ), self.ϵ, self.env)
            δ = (
                R
                + self.γ * np.dot(self.θ.T, self.ϕ(S_1))[A_1]
                - np.dot(self.θ.T, self.ϕ(S))[A]
            )

            # Update behaiviour policy weights
            self.u[:, A] = self.u[:, A] + self.β * (δ * self.ϕ(S) - self.u[:, A])
            self.θ[:, A] = self.θ[:, A] + self.α * (
                self.ϕ(S) - self.γ * self.ϕ(S_1)
            ) * np.dot(self.ϕ(S), self.u[:, A])
            S = S_1

        print(f"Total reward: {total_reward}, mean u: {self.u.reshape(-1).mean()}")


# Setup Environment
env = gym.make("CartPole-v0")
# env = gym.make("SimpleCliffworld-v0")
logger.set_level(logger.INFO)
outdir = tempfile.mkdtemp()
env.seed(0)

A_set = np.arange(env.action_space.n)

agent_train = GTD(env=env, ϵ=0.1)
for _ in range(200):
    agent_train.episode()
print(f"Final weights:\n{agent_train.θ}\n{agent_train.u}")

agent_test = GTD(env=env, ϵ=0.0)
agent_test.θ = agent_train.θ
agent_test.episode()
