# rl-function-approximation

Examples of approximation methods for RL.

This is a project by [Winder Research](https://WinderResearch.com), a Cloud-Native Data Science consultancy.

Implements:
- [GTD(0)](src/rl_function_approximation/gtd.py)
- [Greedy-GQ](src/rl_function_approximation/greedy_gq.py)

## Usage

```console
pip install rl-function-approximation
```

Or clone this repo and run the files directly.

## Credits

Icons made by [Payungkead](https://www.flaticon.com/authors/payungkead).