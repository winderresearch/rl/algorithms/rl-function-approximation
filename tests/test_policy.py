"""Test policy related functions."""
import gym  # type: ignore
import numpy as np  # type: ignore
from pytest_mock import mocker  # type: ignore

from rl_function_approximation import policy


def test_argmax_ϵ_greedy(mocker: mocker) -> None:
    """test_argmax_ϵ_greedy."""
    π = np.array([10, 1])
    mock_env = mocker.Mock(spec=gym.core.Env)
    mock_env.action_space.sample.return_value = np.int64(42)
    res = policy.argmax_ϵ_greedy(π=π, ϵ=0, env=mock_env)
    assert res == 0
    policy.argmax_ϵ_greedy(π=π, ϵ=1.0, env=mock_env)
    assert len(mock_env.method_calls) > 0


def test_π() -> None:
    """test_π."""
    S = np.array([1, 2])
    θ = np.array([2, 2])
    expected = np.array([6])
    res = policy.π(S, θ, lambda x: x)
    np.testing.assert_array_equal(res, expected)
